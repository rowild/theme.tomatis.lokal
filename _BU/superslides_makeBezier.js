// From: https://github.com/webbiesdk/SudoSlider/blob/master/js/jquery.sudoSlider.js
// From this guy: https://github.com/rdallasgray/bez
// Inlined into my own script to make it shorter.

function makeBezier(coOrdArray) {
    var encodedFuncName = "bez_" + coOrdArray.join("_").replace(/\./g, "p"),
        jqueryEasing = $.easing;

    if (!isFunction(jqueryEasing[encodedFuncName])) {
        var polyBez = function (p1, p2) {
            var A = [0, 0];
            var B = [0, 0];
            var C = [0, 0];

            function bezCoOrd(t, ax) {
                C[ax] = 3 * p1[ax], B[ax] = 3 * (p2[ax] - p1[ax]) - C[ax], A[ax] = 1 - C[ax] - B[ax];
                return t * (C[ax] + t * (B[ax] + t * A[ax]));
            }

            function xDeriv(t) {
                return C[0] + t * (2 * B[0] + 3 * A[0] * t);
            }

            function xForT(t) {
                var x = t, i = 0, z;
                while (++i < 14) {
                    z = bezCoOrd(x, 0) - t;
                    if (mathAbs(z) < 1e-3) break;
                    x -= z / xDeriv(x);
                }
                return x;
            }

            return function (t) {
                return bezCoOrd(xForT(t), 1);
            }
        };
        jqueryEasing[encodedFuncName] = function (x, t, b, c, d) {
            return c * polyBez([coOrdArray[0], coOrdArray[1]], [coOrdArray[2], coOrdArray[3]])(t / d) + b;
        }
    }
    return encodedFuncName;
}
