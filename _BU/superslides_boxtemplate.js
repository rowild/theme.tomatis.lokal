function boxTemplate(obj, reverse, reverseRows, grow, randomize, selectionAlgorithm, flyIn, reveal, roundedGrow) {
    var options = obj.options;
    var boxRows = options.boxrows;
    var boxCols = options.boxcols;
    var totalBoxes = boxRows * boxCols;
    var speed = options.speed / (totalBoxes == 1 ? 1 : 2.5); // To make the actual time spent equal to options.speed.
    var boxes = createLazyBoxes(obj, boxCols, boxRows, !reveal);
    var timeBuff = 0;
    var rowIndex = 0;
    var colIndex = 0;
    var box2DArr = [];
    box2DArr[rowIndex] = [];
    if (reverse) {
        reverseArray(boxes);
    }
    if (randomize) {
        shuffle(boxes);
    }

    for (var i = 0; i < boxes.length; i++) {
        var element = boxes[i];
        box2DArr[rowIndex][colIndex] = element;
        colIndex++;
        if (colIndex == boxCols) {
            if (reverseRows) {
                reverseArray(box2DArr[rowIndex]);
            }
            rowIndex++;
            colIndex = 0;
            box2DArr[rowIndex] = [];
        }
    }

    var boxesResult = [];
    if (selectionAlgorithm == 1) {
        // Rain
        for (var cols = 0; cols < (boxCols * 2) + 1; cols++) {
            var prevCol = cols;
            var boxesResultLine = [];
            for (var rows = 0; rows < boxRows; rows++) {
                if (prevCol >= 0 && prevCol < boxCols) {
                    var rawBox = box2DArr[rows][prevCol];
                    if (!rawBox) {
                        return;
                    }
                    boxesResultLine.push(rawBox);
                }
                prevCol--;
            }
            if (boxesResultLine.length != 0) {
                boxesResult.push(boxesResultLine);
            }
        }
    } else if (selectionAlgorithm == 2) {
        // Spiral
        // Algorithm borrowed from the Camera plugin by Pixedelic.com
        var rows2 = boxRows / 2, x, y, z, n = reverse ? totalBoxes : -1;
        var negative = reverse ? -1 : 1;
        for (z = 0; z < rows2; z++) {
            y = z;
            for (x = z; x < boxCols - z - 1; x++) {
                boxesResult[n += negative] = boxes[y * boxCols + x];
            }
            x = boxCols - z - 1;
            for (y = z; y < boxRows - z - 1; y++) {
                boxesResult[n += negative] = boxes[y * boxCols + x];
            }
            y = boxRows - z - 1;
            for (x = boxCols - z - 1; x > z; x--) {
                boxesResult[n += negative] = boxes[y * boxCols + x];
            }
            x = z;
            for (y = boxRows - z - 1; y > z; y--) {
                boxesResult[n += negative] = boxes[y * boxCols + x];
            }
        }
    } else {
        for (var row = 0; row < boxRows; row++) {
            for (var col = 0; col < boxCols; col++) {
                boxesResult.push([box2DArr[row][col]]);
            }
        }
    }

    if (reveal) {
        obj.goToNext();
    }

    var count = 0;
    for (var i = 0; i < boxesResult.length; i++) {
        var boxLine = boxesResult[i];

        if (!isArray(boxLine)) {
            boxLine = [boxLine];
        }

        for (var j = 0; j < boxLine.length; j++) {
            var lazyBox = boxLine[j];
            (function (lazyBox, delay) {
                function boxAnimationFunction(delay) {
                    var box = lazyBox();
                    var boxChildren = box.children();
                    var orgWidth = box.width();
                    var orgHeight = box.height();
                    var goToWidth = orgWidth;
                    var goToHeight = orgHeight;
                    var orgLeft = parseNumber(box.css("left"));
                    var orgTop = parseNumber(box.css("top"));
                    var goToLeft = orgLeft;
                    var goToTop = orgTop;

                    var childOrgLeft = parseNumber(boxChildren.css("left"));
                    var childOrgTop = parseNumber(boxChildren.css("top"));
                    var childGoToLeft = childOrgLeft;
                    var childGoToTop = childOrgTop;

                    if (flyIn) {
                        var adjustTop;
                        var adjustLeft;

                        if (randomize) {
                            adjustLeft = pickRandomValue([-goToWidth, goToWidth]);
                            adjustTop = pickRandomValue([-goToHeight, goToHeight]);
                        } else {
                            adjustLeft = reverse != reverseRows ? -goToWidth : goToWidth;
                            adjustTop = reverse ? -goToHeight : goToHeight;
                        }


                        var flyDistanceFactor = 1.5;

                        if (reveal) {
                            goToLeft -= adjustLeft * flyDistanceFactor;
                            goToTop -= adjustTop * flyDistanceFactor;
                        } else {
                            box.css({
                                left: orgLeft + adjustLeft * flyDistanceFactor,
                                top: orgTop + adjustTop * flyDistanceFactor
                            });
                        }
                    }

                    if (grow) {
                        if (reveal) {
                            childGoToLeft -= goToWidth / 2;
                            goToLeft += goToWidth / 2;
                            childGoToTop -= goToHeight / 2;
                            goToTop += goToHeight / 2;

                            goToHeight = goToWidth = 0;
                        } else {
                            box.css({left: orgLeft + (goToWidth / 2), top: orgTop + (goToHeight / 2)});
                            boxChildren.css({
                                left: childOrgLeft - goToWidth / 2,
                                top: childOrgTop - goToHeight / 2
                            });

                            box.width(0).height(0);
                            if (roundedGrow) {
                                box.css({borderRadius: mathMax(orgHeight, orgWidth)});
                            }
                        }
                    }


                    if (reveal) {
                        box.css({opacity: 1});
                    }
                    count++;
                    schedule(function () {
                        doc.ready(function () {
                            animate(boxChildren, {left: childGoToLeft, top: childGoToTop}, speed, FALSE, FALSE, obj);
                            animate(box, {
                                opacity: reveal ? 0 : 1,
                                width: goToWidth,
                                height: goToHeight,
                                left: goToLeft,
                                top: goToTop,
                                borderRadius: grow && reveal && roundedGrow ? mathMax(orgHeight, orgWidth) : 0
                            }, speed, FALSE, function () {
                                count--;
                                if (count == 0) {
                                    obj.callback();
                                }
                            }, obj);
                        })
                    }, delay);
                }

                var minWaitTime = 150;
                if (reveal || delay < minWaitTime) {
                    boxAnimationFunction(delay);
                } else {
                    schedule(makeCallback(boxAnimationFunction, [minWaitTime]), delay - minWaitTime);
                }
            })(lazyBox, timeBuff);
        }
        timeBuff += (speed / boxesResult.length) * 1.5;
    }
}





