/*-----------------------------------------
 | Source: http://greensock.com/timelinelite
 |         sourcecode for testimonials (slider)
 -----------------------------------------*/
 
var currentTestimonial = 1;
var totalTestimonials = 0;

jQuery(document).ready(function($) {

    totalTestimonials = $('.footer-blocks .testimonial').length;

    $('.t-button').hover(
        function() {
            TweenMax.to($(this).children('.buttonHover') , 0.25, {autoAlpha:1, overwrite:1 });
        },
        function() {
            TweenMax.to($(this).children('.buttonHover') , 0.5, {autoAlpha:0, overwrite:1 });
        }
    );

    $('.testimonial-arrows .arrow').hover(
        function() {
            TweenMax.to($(this) , 0.25, {autoAlpha:1, overwrite:1 });
        },
        function() {
            TweenMax.to($(this) , 0.5, {autoAlpha:.75, overwrite:1 });
        }
    );

    $(".t-button").click(function () {
        updateTestimonial($(this).index()+1, 1);
    });

    $(".testimonial-arrows .left-arrow").click(function () {
        currentTestimonial--;
        if (currentTestimonial == 0) {
            currentTestimonial = totalTestimonials;
        }
        updateTestimonial(currentTestimonial, 1);
    });
    $(".testimonial-arrows .right-arrow").click(function () {
        currentTestimonial++;
        if (currentTestimonial > totalTestimonials) {
            currentTestimonial = 1;
        }
        updateTestimonial(currentTestimonial, 1);
    });

    updateTestimonial(1, 0);

    // Vertically Align all testimonials
    for (var i = 1; i<= $('.footer-blocks .testimonial').length; i++) {
        var testi = $('.footer-blocks .testimonial:nth-child('+i+')');
        TweenMax.to(testi, 0, {paddingTop:(300-testi.height())/2});
    }

});

function updateTestimonial(index, speed) {
    currentTestimonial = index;
    TweenMax.to(jQuery('.footer-blocks .testimonial'), .25*speed, {autoAlpha:0, overwrite:1});
    TweenMax.to(jQuery('.footer-blocks .testimonial:nth-child('+index+')'), .5*speed, {autoAlpha:1});
    TweenMax.to(jQuery('.t-button .buttonActive'), 0.25*speed, {autoAlpha:0, overwrite:1});
    TweenMax.to(jQuery('.t-button:nth-child('+index+') .buttonActive'), 0.5*speed, {autoAlpha:1});
}
