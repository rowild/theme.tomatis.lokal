/*-----------------------------------------
 | Google Maps
 -----------------------------------------*/

/*-----------------------------------------
 | Custom Fade & Slide in / out to circumvent
 | the display:none problem with jquery's
 | slideToggle etc
 -----------------------------------------*/

// Accordion toggle extension for Google Map, indexed search etc
// add something that makes the site scroll upwards on open
$.fn.FadeSlideToggle = function (elem, speed, height) {
    if ($(elem).height() < 50) {
        //$(elem).find('h5').addClass('active').css('color', '#f9300f');
        //$(elem).find('h5 span').removeClass('ui-icon-double-arrow-closed').addClass('ui-icon-double-arrow-open');

        $('body').stop().animate({
            'scrollTop': $('footer').offset().top
        }, 1500, 'easeInOutExpo');

        return $(elem).animate({
            'height':height
        }, speed || 450);
    } else {
        //$(elem).find('h5').removeClass('active').css('color', '#555555');
        //$(elem).find('h5 span').removeClass('ui-icon-double-arrow-open').addClass('ui-icon-double-arrow-closed');

        return $(elem).animate({
            'height':0
        }, speed || 450);
    }
};

$(document).ready(function(){

    var map = new GMaps({
        div: "#map",
        lat: 48.2283324,
        lng: 16.3538907,
        zoom: 15,
        width: "100%",
        height: "360px"
    });

    map.addMarker({
        lat: 48.2283324,
        lng: 16.3538907,
        title: 'TOMATIS-Trainerin Judith Kröll',
        infoWindow: {
            content: '<p>Tomatis mit Judith Kröll</p>'
        }
    });

    $('#map-toggler').on('click', function(e){
        e.preventDefault();
        $(this).FadeSlideToggle('#map-wrapper', 450, 320);
    });

});
