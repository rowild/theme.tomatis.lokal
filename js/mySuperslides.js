// Superslider extra effects
//$.fn.superslides.fx = $.extend({
//    flip: function (orientation, complete) {
//        console.log(orientation);
//        complete();
//    }
//}, $.fn.superslides.fx);


$(document).ready(function () {

    /*-----------------------------------------
     | Image Slider superslides.js
     -----------------------------------------*/

    var $slides = $('#slides');

    $slides.superslides({
        inherit_width_from: window,
        inherit_height_from: window,
        hashchange: false,
        play: false,
        animation: 'fade', // slide
        animation_speed: 2000,
        animation_easing: 'easeInOutQuad',
        pagination: true
        //slide_easing: "easeInOutCubic",
        //scrollable: true
    });

    //var $portraitSlides = $('#portrait-slides');
    //$portraitSlides.superslides({
    //    play: true,
    //    animation: 'fade',
    //    animation_speed: 2000,
    //    animation_easing: 'easeInOutQuad',
    //    pagination: false,
    //    navigation: false
    //});

    // before slide animation begins
    //$slides.on('animating.slides', function () {
    //    var cur = $(this).superslides('current');
    //    console.log("Begin animation on ", $('.slides-container li:nth-child(' + cur + ')').css('border','15px yellow solid'));
    //});

    // after slide animation ends
    //$slides.on('animated.slides', function () {
    //    //var cur = $(this).superslides('current');
    //    //var txt = $('.slides-container li:nth-child(' + cur + ')').text();
    //    //console.log(txt);
    //});
    //
    //$slides.on('init.slides', function () {
    //    console.log("Init slides", $(this).superslides('current'));
    //});
    //
    //$slides.on('started.slides', function () {
    //    console.log("Started slides", $(this).superslides('current'));
    //});
});
