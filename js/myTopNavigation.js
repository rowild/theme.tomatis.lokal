/*-----------------------------------------
 | Makes the top navigation collapse on
 | scrollDown
 -----------------------------------------*/

$(document).ready(function() {

    /*-----------------------------------------
     | jQuery to collapse the navbar on scroll
     -----------------------------------------*/

    var $navTopBar = $('nav.top-bar');

    if ($navTopBar.length) {
        // if the page is in a "scrolled status", adjust the menus immediately
        if ($navTopBar.offset().top > 115) {
            $navTopBar.addClass("top-nav-collapse");

            // slider-navigation and -pagination are added by JS and, at
            // this time, will not be recognized by this script. Therefore
            // call them directly in the if-statement
            $('nav.slides-pagination').addClass("slider-pagination-collapse");
            $('nav.slides-navigation').addClass("slider-pagination-collapse");
        }

        $(window).scroll(function() {
            if ($navTopBar.offset().top > 115) {
                $navTopBar.addClass("top-nav-collapse");
                $('nav.slides-pagination').addClass("slider-pagination-collapse");
                $('nav.slides-navigation').addClass("slider-pagination-collapse");
            } else {
                $navTopBar.removeClass("top-nav-collapse");
                $('nav.slides-pagination').removeClass("slider-pagination-collapse");
                $('nav.slides-navigation').removeClass("slider-pagination-collapse");
            }
        });

        // Menus - click on the links, get hash, scroll to anchor tag
        // jQuery for page scrolling feature - requires jQuery Easing plugin
        $('.top-bar-section').find('li a').bind('click', function(event) {
            event.preventDefault();
            // Get the current target hash
            var target = this.hash;

            $('body').stop().animate({
                'scrollTop': $(target).offset().top - 45
            }, 1500, 'easeInOutExpo', function() {
                //window.location.hash = $anchor.attr('href');
                //window.location.hash = target;
            });
            return false;
        });
    } else {
        //console.log("No top nav bar found");
    }

    //$('.page-scroll a').bind('click', function(event) {
    //    var $anchor = $(this);
    //    console.log("attr. ", $anchor.attr('href'));
    //
    //    //$('html, body').stop().animate({
    //    //    scrollTop: $($anchor.attr('href')).offset().top
    //    //}, 1500, 'easeInOutExpo');
    //    event.preventDefault();
    //});

});
