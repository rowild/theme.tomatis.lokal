var win = $(window);

$(document).ready(function () {
    var container = $("#container"),
        sudoSlider = $("#slider").sudoSlider({
            effect: "blinds1,blinds2,fold,reveal,slice,sliceReverse,sliceRandom,sliceReveal,sliceRevealRandom,sliceRevealReverse,sliceFade,zip,unzip,boxRandom,fade,fadeOutIn", // Up, Right, Down, Left
            responsive: true,
            prevNext: false,
            prevHtml: '<a href="#" class="prevBtn"><i class="fa fa-chevron-left"><span class="hide">previous</span></i></a>',
            nextHtml: '<a href="#" class="nextBtn"><i class="fa fa-chevron-right"><span class="hide">next</span></i></a>',
            continuous: true, // last and first image connect again; keeps pres and next btn visible all the time
            autoHeight: false,
            touch: true,
            customLink: ".sudoSliderLink", // active state of the dot navigation
            updateBefore: true, // dot navigation: active state after animation (false) or before animation (true)
            speed: 2500,
            auto: true,
            beforeAniFunc: function(t){
                $('div.descrip-text #anistate').text('Animating to slide ' + t).show(600);
            },
            afterAniFunc: function(t){
                $('div.descrip-text #anistate').hide(400);
                $('div.descrip-text #slidenumber').text(t);
                var text = $(this).children().attr('src');
                $('div.descrip-text #slidehtml').text(text);
            }
        });

    win.on("resize blur focus", function () {
        var height = win.height();
        sudoSlider.height(height);
        container.height(height);
    }).resize();

    sudoSlider.find(".slide").each(function () {
        var slide = $(this),
            imageSrc = slide.attr("data-background");
        if (!imageSrc) {
            return;
        }
        $("<img />").attr("src", imageSrc).properload(function () {
            var backgroundImage = $(this),
                imageHeight = backgroundImage[0].naturalHeight,
                imageWidth = backgroundImage[0].naturalWidth;

            if (!imageHeight) {
                var img = new Image();
                img.src = imageSrc;
                imageWidth = img.width;
                imageHeight = img.height;
            }
            var aspectRatio = imageWidth / imageHeight;
            backgroundImage.appendTo(slide);
            slide.css({
                zIndex: 0
            });

            backgroundImage.css({
                position: "absolute",
                zIndex: -1,
                top: 0,
                left: 0
            });

            win.on("resize blur focus", function () {
                var sliderWidth = sudoSlider.width(),
                    sliderHeight = sudoSlider.height();
                if ((sliderWidth / sliderHeight) < aspectRatio) {
                    var leftMargin = ((sliderWidth - (sliderHeight * aspectRatio)) / 2) + "px";
                    backgroundImage.css({
                        top: 0,
                        left: leftMargin,
                        width: sliderHeight * aspectRatio,
                        height: sliderHeight
                    });
                } else {
                    backgroundImage.css({
                        left: 0,
                        top: ((sliderHeight - (sliderWidth / aspectRatio)) / 2) + "px",
                        height: sliderWidth / aspectRatio,
                        width: sliderWidth
                    });
                }
            }).resize();

        }, true);
    });
});
