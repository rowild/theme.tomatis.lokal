$(document).ready(function () {

    // Spinner & Preloader
    var opts = {
            lines: 13, // The number of lines to draw
            length: 20, // The length of each line
            width: 10, // The line thickness
            radius: 30, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#eee', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: true, // Whether to render a shadow
            hwaccel: true, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent
            left: '50%' // Left position relative to parent
        },
        target = document.getElementById('preloader');
    spinner = new Spinner(opts).spin(target);

    $(window).load(function () {
        $("#preloader").delay(250).fadeOut("slow", function () {
            spinner.stop();
        });
        $("#load").delay(250).fadeOut("slow");
    });

});
