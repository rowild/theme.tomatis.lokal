"use strict";

/*-----------------------------------------
 | Circle Progress Bars
 -----------------------------------------*/

$(document).ready(function () {

	$.circleProgress.defaults.size = 150;
	$.circleProgress.defaults.thickness = 10;
	$.circleProgress.defaults.fill = {color: "#ef9320"}; //gradient: ["red", "orange"],
	$.circleProgress.defaults.emptyFill = "rgb(51, 51, 51)";
	$.circleProgress.defaults.animation = { duration: 4500, ease: "circleProgressEase"};
	$.circleProgress.defaults.startAngle = Math.PI;
	$.circleProgress.defaults.animationStartValue = 0.0;
	$.circleProgress.defaults.reverse = false;

	var $anwendungsgebiete_circles = $('#anwendungsgebiete_circles'),
		$behandlung_circles = $('#behandlung_circles'),
		$window = $(window),
		circlesAnwendungsgebieteAnimated = true,
		circlesBehandlungAnimated = true;

	/*
	 Initial draw - this happens because of the chaotic slider that
	 initializes the draw even though the following scripts checks for position in viewport;
	 so redraw later
	 */
	drawAnwendungsCircles();
	drawBehandlungsCricles();

	// start animating circles only when in viewport (chaotic!)

	$window.scroll(function () {
		//console.log("$anwendungsgebiete_circles.offset().top =", $anwendungsgebiete_circles.offset().top);
		//console.log("$window.scrollTop =", $window.scrollTop());
		//console.log("$anwendungsgebiete_circles.offset().top - $window.height() =", $anwendungsgebiete_circles.offset().top - $window.height());
		//console.log("<br>\n");
		//console.log($anwendungsgebiete_circles.offset().top - $window.height(), " < ", $window.scrollTop(), " ???");

		// ANWENDUNGEN CIRCLES
		if (($anwendungsgebiete_circles.offset().top - $window.height()) < $window.scrollTop()) {
			if (circlesAnwendungsgebieteAnimated === false) {
				//console.log("-------> Animate circles now\n");

				circlesAnwendungsgebieteAnimated = true;

				drawAnwendungsCircles();
			}
		} else {
			//console.log("\n------> reset\n");
			circlesAnwendungsgebieteAnimated = false;
		}

		// BEHANDLUNG CIRCLES
		if(($behandlung_circles.offset().top - $window.height()) < $window.scrollTop()){
			if(circlesBehandlungAnimated === false) {
				console.log("-------> Animate circles now\n");
				circlesBehandlungAnimated = true;

				drawBehandlungsCricles();
			}
		} else {
			console.log("\n------> reset\n");
			circlesBehandlungAnimated = false;
		}

	});


});


function drawBehandlungsCricles(){
	$('#circle_analyse_01').circleProgress({
		value:.15
	}).on('circle-animation-progress', function(event, progress, stepValue) {
		$(this).find('span.percentage').html(String(stepValue.toFixed(2)).substr(2) + '%');
	});

	$('#circle_session_01').circleProgress({
		value: .45 - .15,
		startAngle: (.15 * 2 - 1) * Math.PI
	}).on('circle-animation-progress', function(event, progress, stepValue) {
		$(this).find('span.percentage').text(String(stepValue.toFixed(2)).substr(2) + '%');
	});

	$('#circle_analyse_02').circleProgress({
		value: .55 - .45,
		startAngle: (.45 * 2 - 1) * Math.PI
	}).on('circle-animation-progress', function(event, progress, stepValue) {
		$(this).find('span.percentage').text(String(stepValue.toFixed(2)).substr(2) + '%');
	});

	$('#circle_session_02').circleProgress({
		value:.85 - .55,
		startAngle : (.55 * 2 - 1) * Math.PI
	}).on('circle-animation-progress', function(event, progress, stepValue) {
		$(this).find('span.percentage').text(String(stepValue.toFixed(2)).substr(2) + '%');
	});

	$('#circle_abschluss').circleProgress({
		value:1 - .85,
		//animationStartValue : 0.836,
		startAngle : (.85 * 2 - 1) * Math.PI
	}).on('circle-animation-progress', function(event, progress, stepValue) {
		$(this).find('span.percentage').text(String(stepValue.toFixed(2)).substr(2) + '%');
	});
}


function drawAnwendungsCircles(){
	$('#circle01').circleProgress({ value: 0.65, animation:1500 });
	$('#circle02').circleProgress({ value: 0.25, animation:1500 });
	$('#circle03').circleProgress({ value: 0.40, animation:1500 });
	$('#circle04').circleProgress({ value: 0.89, animation:1500 });
}

// https://github.com/moagrius/isOnScreen/blob/master/jquery.isonscreen.js
/*(function ($) {

 $.fn.isOnScreen = function(x, y){

 if(x == null || typeof x == 'undefined') x = 1;
 if(y == null || typeof y == 'undefined') y = 1;

 var win = $(window);

 var viewport = {
 top : win.scrollTop(),
 left : win.scrollLeft()
 };
 viewport.right = viewport.left + win.width();
 viewport.bottom = viewport.top + win.height();

 var height = this.outerHeight();
 var width = this.outerWidth();

 if(!width || !height){
 return false;
 }

 var bounds = this.offset();
 bounds.right = bounds.left + width;
 bounds.bottom = bounds.top + height;

 var visible = (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

 if(!visible){
 return false;
 }

 var deltas = {
 top : Math.min( 1, ( bounds.bottom - viewport.top ) / height),
 bottom : Math.min(1, ( viewport.bottom - bounds.top ) / height),
 left : Math.min(1, ( bounds.right - viewport.left ) / width),
 right : Math.min(1, ( viewport.right - bounds.left ) / width)
 };

 return (deltas.left * deltas.right) >= x && (deltas.top * deltas.bottom) >= y;

 };

 })(jQuery);*/
