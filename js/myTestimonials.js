/*-----------------------------------------
 | All Testimonials
 -----------------------------------------*/

$(document).ready(function($) {
    // Variables for the testimonial slides and navigation
    var tm = $('#testimonials'),
        showTestimonial = false,
        tmNav = $('.testimonial-navigation'),
        tmNavPrev = tmNav.find('a.prev'),
        tmNavNext = tmNav.find('a.next'),
        currentTestimonial = 1,
        totalTestimonials = tm.find('.testimonial').length;


    function addShowToTestimonial(elem){
        //console.log("Add Elem =", elem);

        if(showTestimonial === false) {
            $(elem).css({'opacity':'1', 'transition':'opacity 1.2s ease-in-out'});
            $('.testimonial-navigation').find('a').css({'opacity':'1', 'transition':'opacity 1.2s ease-in-out'});
            showTestimonial = true;
        }
    }

    function removeShowFromTestimonial(elem){
        //console.log("Rem Elem =", elem);

        if(showTestimonial === true) {
            $(elem).css({'opacity':'0', 'transition':'opacity .1.2s ease-in-out'});
            $('.testimonial-navigation').find('a').css({'opacity':'1', 'transition':'opacity 1.2s ease-in-out'});
            showTestimonial = false;
        }
    }

    // See extra file "myWinScroll"

    // fadeIn / fadeOut testimonials based on scroll position
    var $win = $(window);
    $win.scroll(function(){
        var wh = $win.height(),
            winPos = $win.scrollTop();

        if((tm.offset().top - winPos + (tm.height()*.5)) < wh ) {
            addShowToTestimonial('#testimonials-wrapper .testimonial:nth-child(' + currentTestimonial + ')');
        } else {
            if(showTestimonial === true){
                console.log("remove testimonial " + currentTestimonial);
                removeShowFromTestimonial('#testimonials-wrapper .testimonial:nth-child(' + currentTestimonial + ')');
            }
        }
    });

    // testimonial navigation
    tmNavPrev.on('click', function(e){
        e.preventDefault();

        // fade Out current testimonial
        removeShowFromTestimonial('#testimonials-wrapper .testimonial:nth-child(' + currentTestimonial + ')');

        // adjust counter with respect to the array length of testimonials
        if(currentTestimonial === 1){
            currentTestimonial = totalTestimonials;
        } else {
            currentTestimonial--;
        }

        // fade in previous testimonial
        addShowToTestimonial('#testimonials-wrapper .testimonial:nth-child(' + currentTestimonial + ')');
    });

    tmNavNext.on('click', function(e){
        e.preventDefault();

        // fade out current testimonial
        removeShowFromTestimonial('#testimonials-wrapper .testimonial:nth-child(' + currentTestimonial + ')');

        // adjust counter with respect to the array length of testimonials
        if(currentTestimonial === totalTestimonials){
            currentTestimonial = 1;
        } else {
            currentTestimonial++;
        }

        // fade in next testimonial
        addShowToTestimonial('#testimonials-wrapper .testimonial:nth-child(' + currentTestimonial + ')');
    });

});

