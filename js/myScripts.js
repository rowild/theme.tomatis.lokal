/*-----------------------------------------
 | My Scripts
 -----------------------------------------*/

// Init Foundation
$(document).foundation();

$(document).ready(function () {
	// init WOW
	new WOW().init();

	// Scroll to anchor tags
	$('#slides').find('a.read-more').on('click', function (e) {
		e.preventDefault();

		var $hash = $(this.hash),
			hashOffsetTop = $hash.offset().top,
			navH = $('.top-bar').height(),
			pos = hashOffsetTop - navH;

		$('body').animate({
			scrollTop: pos
		}, 750, "easeInOutQuint");
	});

	// LOGO - on click scroll to top
	$('.top-bar h1').click(function (e) {
		e.preventDefault();

		$('body').animate({
			scrollTop: 0
		}, 750, "easeInOutQuint");

	});

});
